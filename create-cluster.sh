#!/bin/bash

if ! [[ -e debian10.qcow2 ]]; then
	echo "There is no base debian 10 VM installed on debian10.qcow2 !!"
	echo "Please create a clean debian 10 install in that file before creating cluster"	
	exit 1
fi

if [[ -z "$1" ]]; then
	echo "You must pass as argument how many nodes will have the cluster!"
	echo "For example: ./create-cluster.sh 4"
	exit 1
fi

if [[ -e ./images/cluster1.qcow2 ]]; then
	echo "There is a cluster disks already at images folder  !!"
	echo "Do you want to remove it?? (yes/no)"
	read userinput
	if [[ "$userinput" == "yes" ]]; then
		rm -rf ./images/*.qcow2
	else
		exit 1
	fi
fi

apt install libguestfs-tools -y

# simulate 10G 9000 MTU network
virsh net-define nets/drbd-net.xml
virsh net-autostart drbd
virsh net-start drbd
virsh net-autostart default
virsh net-start default


for ((i=1; i<=$1; i++)); do
	virsh destroy cluster$i
	virsh undefine cluster$i
	sleep 2
	qemu-img create -b $(pwd)/debian10.qcow2 -f qcow2 -F qcow2 ./images/cluster$i.qcow2
	qemu-img create -f qcow2 ./images/cluster$i-d1.qcow2 10G
	qemu-img create -f qcow2 ./images/cluster$i-d2.qcow2 10G

	virt-install --name=cluster$i \
	--wait=4 \
	--vcpus=2 \
	--memory=2000 \
	--boot hd \
	--disk path=./images/cluster$i.qcow2,format=qcow2 \
	--disk path=./images/cluster$i-d1.qcow2,format=qcow2 \
	--disk path=./images/cluster$i-d2.qcow2,format=qcow2 \
	--network network=default \
	--network network=drbd \
	--os-type=linux \
	--os-variant=debian10 &
	sleep 4
	virsh destroy cluster$i
	sleep 2
	echo $i > "/tmp/hostnumber"
	virt-copy-in -d cluster$i /tmp/hostnumber /var/lib/
	virt-copy-in -d cluster$i root/etc /
	virt-copy-in -d cluster$i root/var /

	guestfish -a ./images/cluster$i.qcow2 <<_EOF_ 
run 
mount /dev/sda1 /
ln-s /lib/systemd/system/base_setup.service /etc/systemd/system/multi-user.target.wants/base_setup.service
_EOF_
done

exit 0

